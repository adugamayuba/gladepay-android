package com.gladepay.androidexample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.gladepay.android.Card;
import com.gladepay.android.Charge;
import com.gladepay.android.Gladepay;
import com.gladepay.android.GladepaySdk;
import com.gladepay.android.PTransaction;
import com.gladepay.android.exceptions.ExpiredAccessCodeException;

import org.json.JSONException;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static final String NAME_OF_CLASS = "MAIN-ACTIVITY";

    boolean isLiveServer = false;


    // Set this to a public key
    private String merchantId = "GP0000001";
    // Set the private key
    private String merchantKey = "123456789";

    EditText mEditCardNum;
    EditText mEditCVC;
    EditText mEditExpiryMonth;
    EditText mEditExpiryYear;

    TextView mTextError;

    ProgressDialog dialog;
    private TextView mTextReference;
    private Charge charge;
    private PTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditCardNum = findViewById(R.id.edit_card_number);
        mEditCVC = findViewById(R.id.edit_cvc);
        mEditExpiryMonth = findViewById(R.id.edit_expiry_month);
        mEditExpiryYear = findViewById(R.id.edit_expiry_year);

        Button mButtonPerformTransaction = findViewById(R.id.button_perform_transaction);

        mTextError = findViewById(R.id.textview_error);

        mTextReference = findViewById(R.id.textview_reference);

        //initialize sdk
        GladepaySdk.initialize(getApplicationContext());
        GladepaySdk.setLiveStatus(isLiveServer);
        GladepaySdk.setMerchantId(merchantId);
        GladepaySdk.setMerchantKey(merchantKey);

        if(mButtonPerformTransaction == null){
            Log.i(getClass().getSimpleName(), "NULL MBUTTON PERFORMED TRANSACTION");
        }else{
            Log.i(getClass().getSimpleName(), "NOT-NULL MBUTTON PERFORMED TRANSACTION");
        }

        //add the Click Listener
        mButtonPerformTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    startAFreshCharge();
                } catch (Exception e) {
                    MainActivity.this.mTextError.setText(String.format("An error occurred while charging card: %s %s", e.getClass().getSimpleName(), e.getMessage()));

                }
            }
        });


    }

    private void startAFreshCharge() {
        // initialize the charge
        charge = new Charge();
        charge.setCard(retrieveAndPopulateCardFromForm());

        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Performing transaction... please wait");
        dialog.show();

            charge.setAmount(20000);
            charge.setEmail("support@gladepay.com");
            charge.setReference("ChargedFromAndroid_" + Calendar.getInstance().getTimeInMillis());
            try {
                charge.putCustomField("Charged From", "Android SDK");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            chargeCard();
    }

    private void dismissDialog() {
        if ((dialog != null) && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    private void chargeCard() {
        transaction = null;

        GladepaySdk.chargeCard(MainActivity.this, charge, new Gladepay.TransactionCallback() {
            // This is called only after transaction is successful
            @Override
            public void onSuccess(PTransaction transaction) {
                dismissDialog();

                MainActivity.this.transaction = transaction;
                mTextError.setText(" ");
                Toast.makeText(MainActivity.this, transaction.getTransactionRef(), Toast.LENGTH_LONG).show();
                updateTextViews();
//                new verifyOnServer().execute(transaction.getTransactionRef());
            }

            // This is called only before requesting OTP
            // Save reference so you may send to server if
            // error occurs with OTP
            // No need to dismiss dialog
            @Override
            public void beforeValidate(PTransaction transaction) {
                MainActivity.this.transaction = transaction;
                Toast.makeText(MainActivity.this, transaction.getTransactionRef(), Toast.LENGTH_LONG).show();
                updateTextViews();
            }

            @Override
            public void onError(Throwable error, PTransaction transaction) {
                // If an access code has expired, simply ask your server for a new one
                // and restart the charge instead of displaying error
                MainActivity.this.transaction = transaction;
                if (error instanceof ExpiredAccessCodeException) {
                    MainActivity.this.startAFreshCharge();
                    MainActivity.this.chargeCard();
                    return;
                }

                dismissDialog();

                if (transaction.getTransactionRef() != null) {
                    Toast.makeText(MainActivity.this, transaction.getTransactionRef() + " concluded with error: " + error.getMessage(), Toast.LENGTH_LONG).show();
                    mTextError.setText(String.format("%s  concluded with error: %s %s", transaction.getTransactionRef(), error.getClass().getSimpleName(), error.getMessage()));
                } else {
                    Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    mTextError.setText(String.format("Error: %s %s", error.getClass().getSimpleName(), error.getMessage()));
                }
                updateTextViews();
            }

        });
    }

    private void updateTextViews() {
        if (transaction.getTransactionRef() != null) {
            mTextReference.setText(String.format("Reference: %s", transaction.getTransactionRef() + " " + transaction.getMessage()));
        } else {
            mTextReference.setText("No transaction");
        }
    }

   /* private class verifyOnServer extends AsyncTask<String, Void, String> {
        private String reference;
        private String error;

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                MainActivity.this.mTextError.setText(String.format("Gateway response: %s", result));

            } else {
                MainActivity.this.mTextError.setText(String.format("There was a problem verifying %s on the backend: %s ", this.reference, error));
                dismissDialog();
            }
        }

        @Override
        protected String doInBackground(String... reference) {
            try {
                this.reference = reference[0];
                return null;
            } catch (Exception e) {
                error = e.getClass().getSimpleName() + ": " + e.getMessage();
            }
            return null;
        }
    }*/


    /**
     * Method to validate the form, and set errors on the edittexts.
     */
    private Card retrieveAndPopulateCardFromForm() {
        //validate fields
        Card card;

        String cardNum = mEditCardNum.getText().toString().trim();

        //build card object with ONLY the number, update the other fields later
        card = new Card.CardBuilder(cardNum, 0, 0, "").build();
        String cvc = mEditCVC.getText().toString().trim();
        //update the cvc field of the card
        card.setCvc(cvc);

        //validate expiry month;
        String sMonth = mEditExpiryMonth.getText().toString().trim();
        int month = 0;
        try {
            month = Integer.parseInt(sMonth);
        } catch (Exception ignored) {
        }

        card.setExpiryMonth(month);

        String sYear = mEditExpiryYear.getText().toString().trim();
        int year = 0;
        try {
            year = Integer.parseInt(sYear);
        } catch (Exception ignored) {
        }
        card.setExpiryYear(year);

        return card;
    }
}
