package com.gladepay.android.exceptions;

public class ChargeException extends GladepayException {
    public ChargeException(String message){
        super(message);
    }

}
