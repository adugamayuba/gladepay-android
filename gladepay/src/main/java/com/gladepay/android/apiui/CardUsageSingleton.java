package com.gladepay.android.apiui;


import com.gladepay.android.Card;

public class CardUsageSingleton {
    private static CardUsageSingleton instance = new CardUsageSingleton();
    private Card card = null;

    private CardUsageSingleton() {
    }

    public static CardUsageSingleton getInstance() {
        return instance;
    }

    public Card getCard() {
        return card;
    }

    public CardUsageSingleton setCard(Card card) {
        this.card = card;
        return this;
    }


}