package com.gladepay.android;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.gladepay.android.apiui.OtpActivity;
import com.gladepay.android.apiui.OtpSingleton;
import com.gladepay.android.client.ApiClient;
import com.gladepay.android.exceptions.ProcessingException;
import com.gladepay.android.request.CardChargeRequestBody;
import com.gladepay.android.service.GladepayApiService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentTransactionManager {

    private static final String LOG_TAG = PaymentTransactionManager.class.getSimpleName();
    private static boolean PROCESSING = false;
    private final Charge charge;
    private final Activity activity;
    private final PTransaction paymentTransaction;
    private final Gladepay.TransactionCallback transactionCallback;
    private GladepayApiService apiService;
    private final OtpSingleton osi = OtpSingleton.getInstance();

    private final Callback<JsonObject> serverCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            handleGladepayApiResponse(response);
        }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.e(LOG_TAG, t.getMessage());
            notifyProcessingError(t);
        }
    };
    private CardChargeRequestBody chargeRequestBody;


    void chargeCard() {
        try {
            if (charge.getCard() != null && charge.getCard().isValid()) {
                initiateTransaction();
                sendChargeRequestToServer();

            }else{
                Toast.makeText(this.activity.getApplicationContext(), "Please Enter Valid Card Details", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ce) {
            Log.e(LOG_TAG, ce.getMessage(), ce);
            if (!(ce instanceof ProcessingException)) {
                turnOffProcessing();
            }
            transactionCallback.onError(ce, paymentTransaction);
        }
    }


    public PaymentTransactionManager(Activity activity, Charge charge, Gladepay.TransactionCallback transactionCallback) {

        if (BuildConfig.DEBUG && (activity == null)) {
            throw new AssertionError("please activity cannot be null");
        }
        if (BuildConfig.DEBUG && (charge == null)) {
            throw new AssertionError("charge must not be null");
        }
        if (BuildConfig.DEBUG && (charge.getCard() == null)) {
            throw new AssertionError("please add a card to the charge before calling chargeCard");
        }
        if (BuildConfig.DEBUG && (transactionCallback == null)) {
            throw new AssertionError("transactionCallback must not be null");
        }

        this.activity = activity;
        this.charge = charge;
        this.transactionCallback = transactionCallback;
        this.paymentTransaction = new PTransaction();

    }

    private void sendChargeRequestToServer() {
        try {
            initiateChargeOnServer();
        }catch (Exception ce) {
            Log.e(LOG_TAG, ce.getMessage(), ce);
            notifyProcessingError(ce);
        }
    }

    private void initiateTransaction() throws ProcessingException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (PaymentTransactionManager.PROCESSING) {
            throw new ProcessingException();
        }
        turnOnProcessing();
        apiService = new ApiClient( GladepaySdk.getMerchantId(), GladepaySdk.getMerchantKey(), GladepaySdk.getLiveStatus()).getGladepayApiService();
        chargeRequestBody = new CardChargeRequestBody(charge);
        tLog("INIt transaction");
    }

    private void turnOnProcessing() {
        PaymentTransactionManager.PROCESSING = true;
    }

    private void turnOffProcessing() {
        PaymentTransactionManager.PROCESSING = false;
    }

    private void notifyProcessingError(Throwable t) {
        tLog("Error "+t.getMessage());
        turnOffProcessing();
        transactionCallback.onError(t, paymentTransaction);
    }

    private void initiateChargeOnServer(){
       tLog("InitiateChargeOnServer ChargeRequestServer");
        Call<JsonObject> call = apiService.initiateTransactions(chargeRequestBody.getInitiateParamsJsonObjects());
        call.enqueue(serverCallback);
    }

    private void validateTransactionServer(String otp){
       tLog("validateTransactionServer ValidateRequestServer");
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("validate", paymentTransaction.getTransactionRef(), otp ));
        call.enqueue(serverCallback);
    }
    private void requeryAndverifyTransactionServer(){
       tLog("InitiateChargeOnServer requeryAndVerifyTransactionServer");
        Call<JsonObject> call = apiService.validateTransaction(chargeRequestBody.getParamsJsonObjects("verify", paymentTransaction.getTransactionRef(), null ));
        call.enqueue(serverCallback);
        requeryCount++;
    }

    private int requeryCount = 0;

    private void handleGladepayApiResponse(Response<JsonObject> response) {
        tLog("handleGladepayApiResponse");

        this.paymentTransaction.loadFromResponse(response.body());
        this.paymentTransaction.getStatusId();
        this.paymentTransaction.getTransactionRef();

        JsonObject responseJsonObjectBody;

        if(response == null){
            responseJsonObjectBody = new Gson().fromJson("{\"status\": \"error\"}", JsonObject.class);
        }else{
            responseJsonObjectBody = response.body();
        }

        if(this.paymentTransaction.hasStartedProcessingOnServer()){
            Log.i(getClass().getName()+" ON-RESPONSE: ",  ( new Gson().toJson(response.body())) );
            Log.i(getClass().getName()+" ON-RESPONSE STAT: ",  (this.paymentTransaction.getStatusId() ));
            Log.i(getClass().getName()+" ON-RESPONSE Message:",  ( response.message().toString()) );
            Log.i(getClass().getName()+" ON-RESPONSE E-BDY:",  "[ " + ( response.isSuccessful() +" ]") );
            Log.i(getClass().getName()+" ON-RESPONSE E-BDY:",  ( response.raw().body().toString()) );
        }

//        }else{
//            turnOffProcessing();
//            transactionCallback.onError(new Throwable("Server Processing On Server"), paymentTransaction);
//        }

        String outcome_status = responseJsonObjectBody.get("status").getAsString().toLowerCase();

        switch (outcome_status){
            case "error":
                Log.i("RESPONSE-HR: ", "STATUS:    " + "ERROR");
                break;
            case "success":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "SUCCESS");
                break;
            case "104":
                String message = responseJsonObjectBody.get("message").getAsString();
                if(message.length() >0 ) {
                    paymentTransaction.setMessage(message);
                    Log.i("RESPONSE-HR: ", "STATUS:    " + "104");
                    turnOffProcessing();
                    transactionCallback.onError(new Throwable(message), paymentTransaction);
                    return;
                }
                break;
            case "202":
                if(responseJsonObjectBody.has("apply_auth")){
                    String apply_auth = responseJsonObjectBody.get("apply_auth").getAsString();
                    if(apply_auth.equalsIgnoreCase("otp")){
                        new OtpAsyncTask().execute();
                        return;
                    }
                }
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "202");
                break;
            case "200":

                if(paymentTransaction.hasStartedProcessingOnServer()) {
                    if (requeryCount >= 2) {

                        turnOffProcessing();
                        transactionCallback.onSuccess(paymentTransaction);
                        Log.i("RESPONSE-HR: ", "STATUS:    " + "200");
                        Log.i("RESPONSE-HR: ", "REQUERY-COUNT: > Number of Verification Exceeded");
                        requeryCount = 0;
                        return;
                    }
                }

                if(responseJsonObjectBody.has("cardToken") && responseJsonObjectBody.has("message")){

                    String cardToken = responseJsonObjectBody.get("cardToken").getAsString();
                    message = responseJsonObjectBody.get("message").getAsString();
                    paymentTransaction.setMessage(message);
                    if(message.toLowerCase().contains("success")){
                        Log.i("RESPONSE-HR: ", "ABOUT-REQUERY&VERIFY -MESSAGE");
                        new CountDownTimer(5000, 5000) {
                            public void onFinish() {
                                requeryAndverifyTransactionServer();
                            }

                            public void onTick(long millisUntilFinished) {
                            }
                        }.start();
                        return;
                    }
                }else if(responseJsonObjectBody.has("cardToken") && responseJsonObjectBody.has("bank_message")){

                    String cardToken = responseJsonObjectBody.get("cardToken").getAsString();
                    message = responseJsonObjectBody.get("bank_message").getAsString();
                    paymentTransaction.setMessage(message);
                    if(message.toLowerCase().contains("approved")){
                        Log.i("RESPONSE-HR: ", "ABOUT-REQUERY&VERIFY -BANK_MESSAGE");
                        new CountDownTimer(5000, 5000) {
                            public void onFinish() {
                                requeryAndverifyTransactionServer();
                            }

                            public void onTick(long millisUntilFinished) {
                            }
                        }.start();
                        return;
                    }
                }

                break;
            case "500":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "500");
                break;
            case "400":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "400");
                break;
            case "401":
                Log.i("RESPONSE-HR: ", "STATUS:    "   + "401");
                break;
            case "402":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "402");
                break;
            case "403":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "403");
                break;
            case "300":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "300");
                break;
            case "301":
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "301");
                break;
            default:
                Log.i("RESPONSE-HR: ", "STATUS:    "  + "DEFAULT" +response.body().get("status").getAsString());

        }

        Log.i("RESPONSE-HR: ", new Gson().toJson(responseJsonObjectBody.toString()));

        if (outcome_status.equalsIgnoreCase("202") || outcome_status.equalsIgnoreCase("success")) {
            turnOffProcessing();
            transactionCallback.onSuccess(this.paymentTransaction);
            return;
        }

    }

    private void tLog(String tag){
        Log.i(getClass().getSimpleName(), tag);
    }

    private class OtpAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            Intent i = new Intent(activity, OtpActivity.class);
            activity.startActivity(i);

            synchronized (osi) {
                try {
                    osi.wait();
                } catch (InterruptedException e) {
                    notifyProcessingError(new Exception("OTP entry Interrupted"));
                }
            }

            return osi.getOtp();
        }

        @Override
        protected void onPostExecute(String otp) {
            super.onPostExecute(otp);
            if (otp != null) {
//                chargeRequestBody.setToken(otp);
                PaymentTransactionManager.this.validateTransactionServer(otp); //.validate();
            } else {
                notifyProcessingError(new Exception("You did not provide an OTP"));
            }
        }
    }
}
